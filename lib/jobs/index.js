const fs = require('fs');
const parse = require('csv-parse');
const Wreck = require('wreck');
const cache = require('memory-cache');

const filenameTempo = 'Jeton_InstantExperience_Demande{n}.csv';
let config = {
  V360_APIKEY_INST_EXP: '',
  V360_LINK_INST_EXP: '',
  timeoutValue: '',
  idRcuColumn: '',
  dirnameSAS: '',
  dirnameTempo: '',
};

class InstantExperienceService {
  // START
  static async startJob(myconfig) {
    config = await Object.assign(config, myconfig);
    await this.stepOne(config.dirnameSAS);
    setTimeout(async () => {
      await this.stepTwo(config.dirnameTempo);
    }, config.timeoutValue);
  }

  // STEP ONE
  // GENERATE CSV FILES WITH ONE COLUMN FROM INPUT SAS FILES
  static async stepOne(dirname) {
    try {
      console.log(' Starting the first treatment... ');
      fs.readdir(dirname, (err, filenames) => {
        if (err) {
          console.log(' An error has occured while opening SAS directory ', err);
          return;
        }
        if (filenames.length === 0) {
          console.log(' there\'s no file in SAS directory');
          return;
        }
        let index = 0;
        filenames.forEach((filename) => {
          const isExistSatisfaction = filename.search('Satisfaction');
          const isExistDSCG = filename.search('DSCG_');
          const isExistGDPR = filename.search('_GDPR');
          if (isExistSatisfaction === 0 && isExistDSCG > 11 && isExistGDPR < 0) {
            const idsRcu = [];
            let i = 0;
            let isExist = null;
            const inputFileSASFullPath = config.dirnameSAS.concat(filename);
            fs.createReadStream(inputFileSASFullPath)
              .pipe(parse({ delimiter: ';' }))
              .on('data', (csvrow) => {
                if (i === 0) {
                  isExist = csvrow.indexOf('id_rcu');
                  if (isExist === null || isExist < 0 || isExist !== (config.idRcuColumn - 1)) {
                    console.log('csvrow header error: \'id_rcu\' doesn\'t exist or is not in 54th column');
                  }
                } else if (i !== 0 && csvrow[isExist] !== null && csvrow[isExist] !== '') {
                  idsRcu.push(csvrow[isExist]);
                }
                i += 1;
              })
              .on('end', async () => {
                index += 1;
                this.fileGenerationOneColumn(idsRcu, filename, index);
              });
          }
        });
      });
    } catch (error) {
      console.log(' ERROR in stepOne function : ', error);
    }
  }

  static async fileGenerationOneColumn(data, filename, index) {
    try {
      const fullFilename = filenameTempo.replace('{n}', index);
      const fullPath = config.dirnameTempo.concat(fullFilename);
      const logger = fs.createWriteStream(fullPath, {
        flags: 'a',
      });
      await data.reduce((acc, itt) => {
        logger.write(`${itt}\n`);
        return acc;
      }, []);
      logger.end();
      console.log(' Creation of file : ', fullFilename);
      cache.put(fullFilename, filename);
    } catch (error) {
      console.log(' ERROR in fileGenerationOneColumn function : ', error);
    }
  }

  // STEP TWO
  // GENERATE OUTPUT SAS FILES FROM .CSV.OUT TOKEN FILES
  static async stepTwo(dirname) {
    console.log(' Starting the second treatment... ');
    try {
      let isOutFilesExist = false;
      fs.readdir(dirname, (err, filenames) => {
        if (err) {
          console.log(' An error has occured while opening Temporary (token server) directory ', err);
          return;
        }
        if (filenames.length === 0) {
          console.log(' there\'s no file in temporary (token server) directory');
          return;
        }
        filenames.forEach((filename, index) => {
          const isOutFile = filename.endsWith('.csv.out');
          if (isOutFile) {
            isOutFilesExist = true;
            this.filename = filename.replace('.out', '');
            const idsRcu = [];
            const halfData = [];
            const inputFileTempoFullPath = config.dirnameTempo.concat(filename);
            fs.createReadStream(inputFileTempoFullPath)
              .pipe(parse({ delimiter: ';' }))
              .on('data', (csvrow) => {
                idsRcu.push(csvrow[0]);
                halfData.push({ id: csvrow[0], token: csvrow[1] });
              })
              .on('end', async () => {
                const options = {
                  json: true,
                  headers: {
                    'MM-API-Key': config.V360_APIKEY_INST_EXP,
                    'Content-Type': 'application/json',
                  },
                  rejectUnauthorized: false,
                  payload: idsRcu,
                };
                const url = config.V360_LINK_INST_EXP;
                const promise = Wreck.post(url, options);
                const data = await promise;
                if (data && data.payload && data.payload.length > 0) {
                  const result = await this.recoverConsentements(data.payload, halfData);
                  await this.formatDataSASFileGDPR(result, filename);
                } else {
                  console.log(' there\'s no data in .csv.out file');
                }
              });
          }
          if (index === filenames.length - 1 && isOutFilesExist === false) {
            console.log(' there\'s no file .csv.out in temporary (token server) directory');
          }
        });
      });
    } catch (error) {
      console.log(' ERROR in stepTwo function : ', error);
    }
  }

  static async recoverConsentements(consentements, halfData) {
    try {
      return consentements.reduce((accumulator, element) => {
        const one = {};
        one.id = element.id;
        one.token = halfData.find(_element => _element.id === element.id).token;
        element.consentements.reduce((_accumulator, consent) => {
          if (consent.typeConsentement === 'ENQUETES_SATISFACTION' && consent.canalDiffusion === 'MAIL') {
            one.mail = consent.valeurConsentement;
          } if (consent.typeConsentement === 'ENQUETES_SATISFACTION' && consent.canalDiffusion === 'SMS') {
            one.sms = consent.valeurConsentement;
          }
          return _accumulator;
        }, []);
        accumulator.push(one);
        return accumulator;
      }, []);
    } catch (error) {
      console.log(' ERROR in recoverConsentements function : ', error);
      return null;
    }
  }

  static async formatDataSASFileGDPR(result, filename) {
    try {
      const content = [];
      let headers = '';
      let i = 0;
      let isExist = null;
      const anotherFilename = filename.replace('.out', '');
      const inputFileSASName = cache.get(anotherFilename);
      const outputFileSASName = inputFileSASName.replace('.csv', '_GDPR.csv');
      const outputFileSASFullPath = config.dirnameSAS.concat(inputFileSASName.replace('.csv', '_GDPR.csv'));
      const inputFileSASFullPath = config.dirnameSAS.concat(inputFileSASName);
      fs.createReadStream(inputFileSASFullPath)
        .pipe(parse({ delimiter: ';' }))
        .on('data', (csvrow) => {
          if (i === 0) {
            headers = csvrow.join(';');
            isExist = csvrow.indexOf('id_rcu');
            if (isExist === null || isExist < 0 || isExist !== (config.idRcuColumn - 1)) {
              console.log('csvrow header error: \'id_rcu\' doesn\'t exist or is not in 54th column');
            }
          } else if (i !== 0 && csvrow[isExist] !== null && csvrow[isExist] !== '') {
            const obj = result.find(_element => _element.id === (csvrow[isExist]));
            if (obj && obj.token) {
              const sth = csvrow;
              sth[config.idRcuColumn] = obj.token;
              sth[config.idRcuColumn + 1] = obj.mail;
              sth[config.idRcuColumn + 2] = obj.sms;
              content.push(sth.join(';'));
            }
          }
          i += 1;
        })
        .on('end', async () => {
          await this.generateSASFileGDPR(headers,
            content,
            outputFileSASName,
            outputFileSASFullPath,
            anotherFilename,
            filename);
        });
    } catch (error) {
      console.log(' ERROR in formatDataSASFileGDPR function : ', error);
    }
  }

  static async generateSASFileGDPR(headers,
    data,
    outputFileSASName,
    pathSASOutput,
    tempoFile,
    tempoFileOut) {
    try {
      const logger = fs.createWriteStream(pathSASOutput, {
        flags: 'a',
      });
      logger.write(`${headers}\n`);
      await data.reduce((acc, itt) => {
        logger.write(`${itt}\n`);
        return acc;
      }, []);
      logger.end();
      console.log(` File processing - ${outputFileSASName} is created`);
      await this.removeTempoFiles(config.dirnameTempo, tempoFile, tempoFileOut);
    } catch (error) {
      console.log(' ERROR in generateSASFileGDPR function : ', error);
    }
  }

  static async removeTempoFiles(dirname, tempoFile, tempoFileOut) {
    try {
      fs.readdir(dirname, (err, filenames) => {
        if (err) {
          console.log(' An error has occured while opening Temporary (token server) directory ', err);
          return;
        }
        filenames.forEach((filename) => {
          if (filename === tempoFile) {
            fs.unlink(dirname.concat(tempoFile), (erreur) => {
              if (erreur) throw erreur;
              console.log(` End of file processing - ${tempoFile} is deleted`);
            });
          } else if (filename === tempoFileOut) {
            fs.unlink(dirname.concat(tempoFileOut), (erreur) => {
              if (erreur) throw erreur;
              console.log(` End of file processing - ${tempoFileOut} is deleted`);
            });
          }
        });
      });
    } catch (error) {
      console.log(' ERROR in removeTempoFiles function : ', error);
    }
  }
}

module.exports = InstantExperienceService;
