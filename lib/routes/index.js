module.exports = version => [
  {
    method: 'GET',
    path: '/',
    handler: () => 'Bienvenue dans GPR_H7_batch!',
  },
  {
    method: 'GET',
    path: '/infos',
    handler: () => version,
  },
];
