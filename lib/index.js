const PropertiesReader = require('properties-reader');
const InstantExperienceService = require('./jobs');

const configPath = './config.properties';

class GdprServer {
  static async start() {
    try {
      const properties = PropertiesReader(configPath);
      if (properties
      && properties.get('V360_APIKEY_INST_EXP')
      && properties.get('V360_LINK_INST_EXP')
      && properties.get('timeoutValue')
      && properties.get('idRcuColumn')
      && properties.get('dirnameSAS')
      && properties.get('dirnameTempo')) {
        console.log(' BATCH is running... ');
        const config = {
          V360_APIKEY_INST_EXP: properties.get('V360_APIKEY_INST_EXP'),
          V360_LINK_INST_EXP: properties.get('V360_LINK_INST_EXP'),
          timeoutValue: properties.get('timeoutValue'),
          idRcuColumn: properties.get('idRcuColumn'),
          dirnameSAS: properties.get('dirnameSAS'),
          dirnameTempo: properties.get('dirnameTempo'),
        };
        InstantExperienceService.startJob(await config);
      }
    } catch (err) {
      if (err.errno && err.errno === -4058) {
        console.log('Cannot find config.properties file');
      } else {
        console.log('an error has occured while starting the batch');
      }
      process.exit(1);
    }
  }
}

GdprServer.start();

module.exports = { GdprServer };
