module.exports = {
  dev: '-r e2e/support -r e2e/step-definitions',
  ci: `-r e2e/support -r e2e/step-definitions --format json:e2e/report/${Date.now()}_report.json`,
};
