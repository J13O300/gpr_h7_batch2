const Lab = require('lab');
const proxyquire = require('proxyquire');
const { expect } = require('code');

exports.lab = Lab.script();

const {
  after, before, afterEach, beforeEach, describe, it,
} = exports.lab;

// Mock package.json version
const fakeVersion = '1.0.0-test';
const server = proxyquire('../lib', { '../package': { version: fakeVersion } });

describe('GPR_H7_batch', async () => {
  before(async () => {
    // Do something once before this test suite
  });

  beforeEach(async () => server.start());

  describe('initialization', async () => {
    it('starts the server', async () => {
      expect(server).to.exist();
      return expect(server.info.started).to.not.equal(0);
    });
  });

  describe('/', async () => {
    it('returns a welcome message', async () => {
      // Simulate an HTTP request without using the system socket
      const output = await server.inject('/');
      expect(output).to.exist();
      expect(output.statusCode).to.equal(200);
      return expect(output.result).to.equal('Bienvenue dans GPR_H7_batch!');
    });
  });

  describe('/infos', async () => {
    it('returns the version of the stack', async () => {
      const output = await server.inject('/infos');
      expect(output).to.exist();
      expect(output.statusCode).to.equal(200);
      return expect(output.result).to.equal(fakeVersion);
    });
  });

  afterEach(async () => server.stop());

  after(async () => {
    // Do something once after the test suite
  });
});
