const {
  After, Before, Given, /* When, */ Then,
} = require('cucumber');
const { expect } = require('code');
const Wreck = require('wreck');

// Starts server
const server = require('../../lib');

Before(async () => server.start());

Given('j\'appelle {uri} en {httpMethod}', async function callUri(uri, method) {
  const res = await Wreck.request(method, `http://localhost:${server.info.port}${uri}`);
  const body = await Wreck.read(res);
  this.output = body.toString();
  this.status = res.statusCode;
});

Then('le retour est égal à {string}', function compareOutput(output) {
  expect(this.output).to.equal(output);
});

Then('le code de retour est égal à {int}', function compareStatusCode(code) {
  expect(this.status).to.equal(code);
});

After(async () => server.stop());
