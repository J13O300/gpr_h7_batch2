def TRIGRAMME
def CODEAPPLI
def PROJECTNAME
def TAG
node {
  stage('Git Checkout') {
    // Checkout avec l option noTags à false
    // Par défaut le checkout positionne noTags à true : checkout(scm)
    checkout([
      $class: 'GitSCM',
      branches: scm.branches,
      doGenerateSubmoduleConfigurations: scm.doGenerateSubmoduleConfigurations,
      extensions: [[$class: 'CloneOption', noTags: false, shallow: false, depth: 0, reference: ''],[$class: 'WipeWorkspace']],
      userRemoteConfigs: scm.userRemoteConfigs,
    ])
    def url = sh(returnStdout: true, script: 'git config remote.origin.url').trim()
    def gitProjectName = sh(returnStdout: true, script: "echo '$url' | awk -F'/si2m/' '{print \$2}' |  awk -F'.git' '{print \$1}'").trim()
    TRIGRAMME = sh(returnStdout: true, script: "echo '$gitProjectName' | awk -F'_' '{print \$1}'").trim()
    CODEAPPLI = sh(returnStdout: true, script: "echo '$gitProjectName' | awk -F'_' '{print \$2}'").trim()
    PROJECTNAME = sh(returnStdout: true, script: "echo '$gitProjectName' | awk -F'_' '{print \$3}'").trim()

    TAG = sh(returnStdout: true, script: "git tag | tail -n 1").trim()

    // copie qualimetry files
    sh ('cp $JENKINS_HOME/qualimetry/* $WORKSPACE/')
  }

  docker.withRegistry('https://dtr.docker.si2m.tec') {
    docker.image('test-store/si2m-npm10-build').inside {
      stage('Build') {
        sh 'npm ci --unsafe-perm'
      }
      stage('Qualimetry') {
        parallel(
          "Tests": {
            sh 'npm run test:ci'
          },
          "Lint":{
            sh 'npm run lint'
          }
        )
      }
      stage('EndToEnd') {
        sh 'sed s/es5/esnext/ tsconfig.json -i'
        sh 'npm run e2e'
      }

      if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
        stage('NPM Pack') {
          sh ('npm pack')
        }

        if(env.BRANCH_NAME == 'master' && TRIGRAMME == 'BPP') {
          stage('Publish Nexus') {
            sh ('npm publish --registry "http://vlc3inf013:8081/repository/npm-si2m"')
          }
        }
      }
    }
  }
}
node {
  if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
    stage('Push DTR'){
      if (env.BRANCH_NAME == 'master') {
        CODEENV="2"
        DTRIMAGE="ex${CODEAPPLI}-${TRIGRAMME}-${PROJECTNAME}:${TAG}"
      } else {
        CODEENV="3"
        DTRIMAGE="ex${CODEAPPLI}-${TRIGRAMME}-${PROJECTNAME}:develop"
      }
      withCredentials([usernamePassword( credentialsId: '7c1.03ee-4888-4843-951f-2f7b4f846b8c', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]) {
        docker.withRegistry('https://dtr.docker.si2m.tec/') {
          sh "docker login -u ${USERNAME} -p ${PASSWORD} dtr.docker.si2m.tec"
          def imageDocker= docker.build("repo-store/${DTRIMAGE}",'--no-cache --rm .')
          imageDocker.push()
          if (env.BRANCH_NAME == 'master') {
            imageDocker.push('latest')
          }
        }
      }
    }
  }
}
node {
  if (env.BRANCH_NAME == 'master' || env.BRANCH_NAME == 'develop') {
    stage('Preparation environnement') {
      // copie des certificats pour deploiement UCP
      sh ('cp -r $JENKINS_HOME/docker_ucp_recf $WORKSPACE/docker_cert')
      // variable UCP docker
      DOCKER_HOST= 'tcp://ucp.recf.docker.si2m.tec:443'
    }
    stage('Checkout Dico'){
      sh ("rm -f -r e${CODEENV}${CODEAPPLI}")
      checkout([$class: 'GitSCM', branches: [[name: "*/e${CODEENV}${CODEAPPLI}"]],
      doGenerateSubmoduleConfigurations: false, extensions: [[$class: 'RelativeTargetDirectory', relativeTargetDir: "e${CODEENV}${CODEAPPLI}"]],
      submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'bitbucket-cloud', url: "https://bitbucket.org/si2m/${TRIGRAMME}_dico.git",refspec: "+refs/heads/e${CODEENV}${CODEAPPLI}:refs/remotes/origin/e${CODEENV}${CODEAPPLI}"]]])
    }
    stage ('Deploiement UCP Docker') {
      withEnv(['DOCKER_TLS_VERIFY=1',"DOCKER_CERT_PATH=${WORKSPACE}/docker_cert/","DOCKER_HOST=${DOCKER_HOST}"]) {
        sh "export DTRIMAGE=${DTRIMAGE} && cd e${CODEENV}${CODEAPPLI} && docker-compose config > docker-compose-deploy.yml"
        //sh "export PROJECTNAME=${PROJECTNAME} && export CODEAPPLI=${CODEAPPLI} && export TRIGRAMME=${TRIGRAMME} && cd e${CODEENV}${CODEAPPLI} && docker-compose config > docker-compose-deploy.yml"
        sh "docker stack deploy --prune --compose-file=e${CODEENV}${CODEAPPLI}/docker-compose-deploy.yml e${CODEENV}${CODEAPPLI}-${TRIGRAMME}-${PROJECTNAME}"
      }
    }
  }
}
